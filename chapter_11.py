#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------------ Глава 11 ------------------------------------#

# 11.1 Список чисел ----------------------------------------------------------#
n = int(input())
print(list(range(1, n + 1)))

# 11.1 Список букв -----------------------------------------------------------#
n = int(input())
alphabet = 'abcdefghijklmnopqrstuvwxyz'
print(list(alphabet[:n]))

# 11.3 Все сразу 1 -----------------------------------------------------------#
numbers = [2, 6, 3, 14, 10, 4, 11, 16, 12, 5, 4, 16, 1, 0, 8, 16, 10, 10, 8, 5, 1, 11, 10, 10, 12, 0, 0, 6, 14, 8, 2, 12, 14, 5, 6, 12, 1, 2, 10, 14, 9, 1, 15, 1, 2, 14, 16, 6, 7, 5]

print(len(numbers))
print(numbers[-1])
print(numbers[-1::-1])
if 5 in numbers and 17 in numbers:
    print('YES')
else:
    print('NO')
del numbers[0]
del numbers[-1]
print(numbers)

# 11.3 Список строк ----------------------------------------------------------#
n = int(input())

input_list = list()

for _ in range(n):
    input_list.append(str(input()))

print(input_list)

# 11.3 Алфавит ---------------------------------------------------------------#
alphabet = 'abcdefghijklmnopqrstuvwxyz'

chars_list = list()

for i in range(len(alphabet)):
    chars_list.append(alphabet[i] * (i + 1))

print(chars_list)

# 11.3 Список кубов ----------------------------------------------------------#
n = int(input())

result = list()

for i in range(n):
    num = int(input())
    result.append(num ** 3)
print(result)

# 11.3 Список делителей ------------------------------------------------------#
n = int(input())

result = list()

for i in range(n):
    if n % (i + 1) == 0:
        result.append(i + 1)
print(result)

# 11.3 Суммы двух ------------------------------------------------------------#
n = int(input())

previous = int(input())
result = list()

for _ in range(n - 1):
    current = int(input())
    result.append(current + previous)
    previous = current

print(result)

# 11.3 Удалите нечетные индексы ----------------------------------------------#
n = int(input())

numbers = list()

for _ in range(n):
    numbers.append(int(input()))

del numbers[1::2]

print(numbers)

# 11.3 k-ая буква слова ------------------------------------------------------#
n = int(input())

words = list()

for _ in range(n):
    words.append(input())

k = int(input())
for i in range(n):
    print(words[i][k - 1:k], end='')

# 11.3 Символы всех строк ----------------------------------------------------#
n = int(input())

chars = list()

for _ in range(n):
    chars.extend(input())
print(chars)

# 11.4 Значение функции ------------------------------------------------------#
n = int(input())

numbers = list()
for i in range(n):
    numbers.append(int(input()))

result = list()
for i in range(n):
    result.append((numbers[i] ** 2 + numbers[i] * 2 + 1))

print(*numbers, sep='\n', end='\n\n')
print(*result, sep='\n')

# 11.4 Remove outliers -------------------------------------------------------#
n = int(input())

numbers = list()
for _ in range(n):
    numbers.append(int(input()))

numbers.remove(max(numbers))
numbers.remove(min(numbers))

print(*numbers, sep='\n')

# 11.4 Без дубликатов --------------------------------------------------------#
n = int(input())

str_list = list()

for _ in range(n):
    tmp = input()
    if tmp not in str_list:
        str_list.append(tmp)

print(*str_list, sep='\n')

# 11.4 Google search - 1 -----------------------------------------------------#
n = int(input())

str_list = list()
for _ in range(n):
    str_list.append(input())

query = input()

result = list()
for i in range(n):
    if query.lower() in str_list[i].lower():
        result.append(str_list[i])

print(*result, sep='\n')

# 11.4 Google search - 2 -----------------------------------------------------#
n = int(input())

strings = list()
queries = list()
result = list()

for _ in range(n):
    strings.append(input())

k = int(input())

for _ in range(k):
    queries.append(input())

for i in range(n):
    count = 0
    for j in range(k):
        if queries[j].lower() in strings[i].lower():
            count += 1
    if count == k:
        result.append(strings[i])

print(*result, sep='\n')

# 11.4 Negatives, Zeros and Positives ----------------------------------------#
n = int(input())

numbers = list()
for _ in range(n):
    numbers.append(int(input()))

result = list()

for i in range(n):
    if numbers[i] < 0:
        result.append(numbers[i])

for i in range(n):
    if numbers[i] == 0:
        result.append(numbers[i])

for i in range(n):
    if numbers[i] > 0:
        result.append(numbers[i])

print(*result, sep='\n')

# 11.5 Построчный вывод ------------------------------------------------------#
string = input()
string = string.split()
print(*string, sep='\n')

# 11.5 Инициалы --------------------------------------------------------------#
name = input()
name = name.split()
print(name[0][0], '.', name[1][0], '.', name[2][0], '.', sep='')

# 11.5 Windows OS ------------------------------------------------------------#
path = input()
path = path.split('\\')
print(*path, sep='\n')

# 11.5 Диаграмма -------------------------------------------------------------#
numbers = input()
numbers = numbers.split()

for i in range(len(numbers)):
    print('+' * int(numbers[i]))

# 11.5 Корректный ip-адрес ---------------------------------------------------#
ip = input()

ip = ip.split('.')

is_valid = 'ДА'
for i in range(4):
    if int(ip[i]) < 0 or int(ip[i]) > 255:
        is_valid = 'НЕТ'
        break

print(is_valid)

# 11.5 Добавь разделитель ----------------------------------------------------#
string, separator = input(), input()
string = list(string)
print(separator.join(string))

# 11.5 Количество совпадающих пар --------------------------------------------#
string = input()

string = string.split()
str_len = len(string)
count = 0

for i in range(str_len):
    for j in range(int(i) + 1, str_len):
        if string[i] == string[j]:
            count += 1

print(count)

# 11.6 Все сразу 2 -----------------------------------------------------------#
numbers = [8, 9, 10, 11]

numbers[1] = 17
numbers.extend([4, 5, 6])
del numbers[0]
numbers *= 2
numbers.insert(3, 25)

print(numbers)

# 11.6 Переставить min и max -------------------------------------------------#
string = input()

numbers = string.split()
for i in range(len(numbers)):
    numbers[i] = int(numbers[i])

max_digit = max(numbers)
min_digit = min(numbers)

max_pos = numbers.index(max_digit)
min_pos = numbers.index(min_digit)

numbers[max_pos] = min_digit
numbers[min_pos] = max_digit

print(*numbers, sep=' ')

# 11.6 Количество артиклей ---------------------------------------------------#
string = input()

words = string.lower().split()
count = 0

count += words.count('a')
count += words.count('an')
count += words.count('the')

print('Общее количество артиклей:', count)

# 11.6 Взлом Братства Стали --------------------------------------------------#
n = input()
n = int(n[1:])

for _ in range(n):
    string = input()
    if string[0] == '#':
        continue
    sharp_pos = string.find('#')
    if sharp_pos > 0:
        string = string[:sharp_pos]
    string = string.rstrip()

    print(string)

# 11.6 Сортировка чисел ------------------------------------------------------#
string = input()

numbers = string.split()
for i in range(len(numbers)):
    numbers[i] = int(numbers[i])

numbers.sort()
print(*numbers, sep=' ')

numbers.sort(reverse=True)
print(*numbers, sep=' ')

# 11.7 Списочное выражение 1 -------------------------------------------------#
numbers = [i ** 2 for i in range(1, int(input()) + 1)]
print(*numbers, sep='\n')

# 11.7 В одну строку 1 -------------------------------------------------------#
[print(i) for i in input().split()]

# 11.7 В одну строку 2 -------------------------------------------------------#
[print(i, end='') for i in input() if '0' <= i <= '9']

# 11.7 В одну строку 3 -------------------------------------------------------#
[print(int(i) ** 2, end=' ') for i in input().split() if (int(i) ** 2) % 10 != 4 and (int(i) ** 2) % 2 == 0]

# 11.8 Пузырьковая сортировка ------------------------------------------------#
a = [17, 24, 91, 96, 67, -27, 79, -71, -71, 58, 48, 88, 88, -16, -78, 96, -76, 56, 92, 1, 32, -17, 36, 88, -61, -97, -37, -84, 50, 47, 94, -6, 52, -76, 93, 14, -32, 98, -65, -16, -9, -68, -20, -40, -71, 93, -91, 44, 25, 79, 97, 0, -94, 7, -47, -96, -55, -58, -78, -78, -79, 75, 44, -56, -41, 38, 16, 70, 17, -17, -24, -83, -74, -73, 11, -26, 63, -75, -19, -13, -51, -74, 21, -8, 21, -68, -66, -84, -95, 78, 69, -29, 39, 38, -55, 7, -11, -26, -62, -84]

n = len(a)

for i in range(n - 1):
    sorted = True
    for j in range(n - i - 1):
        if a[j] > a[j + 1]:
            a[j], a[j + 1] = a[j + 1], a[j]
            sorted = False
    if sorted:
        break
print(a)

# 11.8 Сортировка выбором ----------------------------------------------------#
a = [78, -32, 5, 39, 58, -5, -63, 57, 72, 9, 53, -1, 63, -97, -21, -94, -47, 57, -8, 60, -23, -72, -22, -79, 90, 96, -41, -71, -48, 84, 89, -96, 41, -16, 94, -60, -64, -39, 60, -14, -62, -19, -3, 32, 98, 14, 43, 3, -56, 71, -71, -67, 80, 27, 92, 92, -64, 0, -77, 2, -26, 41, 3, -31, 48, 39, 20, -30, 35, 32, -58, 2, 63, 64, 66, 62, 82, -62, 9, -52, 35, -61, 87, 78, 93, -42, 87, -72, -10, -36, 61, -16, 59, 59, 22, -24, -67, 76, -94, 59]
n = len(a)
b = []
while len(a) > 0:
    b.append(min(a))
    a.remove(min(a))
a = b
print(a)

