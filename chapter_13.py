#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------------ Глава 13 ------------------------------------#

# 13.1 Звездный прямоугольник 1 ----------------------------------------------#
def draw_box():
    print('**********')
    for _ in range(12):
        print('*        *')
    print('**********')

draw_box()

# 13.1 Звездный треугольник 1 ------------------------------------------------#
def draw_triangle():
    for i in range(1, 11):
        print('*' * i)


draw_triangle()

# 13.2 Звездный треугольник --------------------------------------------------#
def draw_triangle(fill, base):
    top = (base // 2) + 1
    bottom = (base // 2)
    for i in range(1, top + 1):
        print(fill * i)
    for i in range(bottom, 0, -1):
        print(fill * i)


fill = input()
base = int(input())

draw_triangle(fill, base)

# 13.2 ФИО -------------------------------------------------------------------#
def print_fio(name, surname, patronymic):
    print(surname[0].upper() + name[0].upper() + patronymic[0].upper())


name, surname, patronymic = input(), input(), input()

print_fio(name, surname, patronymic)

# 13.2 Сумма цифр ------------------------------------------------------------#
def print_digit_sum(num):
    num = [int(i) for i in str(num)]
    print(sum(num))


n = int(input())

print_digit_sum(n)

# 13.4 Конвертер километров --------------------------------------------------#
def convert_to_miles(km):
    return km * 0.6214


num = int(input())

print(convert_to_miles(num))

# 13.4 Количество дней -------------------------------------------------------#
def get_days(month):
    if month == 2:
        return 28
    elif month == 4 or month == 6 or month == 9 or month == 11:
        return 30
    else:
        return 31


num = int(input())

print(get_days(num))

# 13.4 Делители 1 ------------------------------------------------------------#
def get_factors(num):
    result = []
    for i in range(num):
        if n % (i + 1) == 0:
            result.append(i + 1)
    return result


n = int(input())

print(get_factors(n))

# 13.4 Делители 2 ------------------------------------------------------------#
def number_of_factors(num):
    count = 0
    for i in range(num):
        if n % (i + 1) == 0:
            count += 1
    return count


n = int(input())

print(number_of_factors(n))

# 13.4 Найти всех ------------------------------------------------------------#
def find_all(target, symbol):
    target = list(target)
    result = []
    for i in range(len(target)):
        if target[i] == symbol:
            result.append(i)
    return result


s = input()
char = input()

print(find_all(s, char))

# 13.4 Merge lists 1 ---------------------------------------------------------#
def merge(list1, list2):
    result = list1 + list2
    result.sort()
    return result


numbers1 = [int(c) for c in input().split()]
numbers2 = [int(c) for c in input().split()]

print(merge(numbers1, numbers2))

# 13.4 Merge lists 2 ---------------------------------------------------------#
def quick_merge(list1, list2):
    result = []

    p1 = 0
    p2 = 0

    while p1 < len(list1) and p2 < len(list2):
        if list1[p1] <= list2[p2]:
            result.append(list1[p1])
            p1 += 1
        else:
            result.append(list2[p2])
            p2 += 1

    if p1 < len(list1):
        result += list1[p1:]
    if p2 < len(list2):
        result += list2[p2:]

    return result


n = int(input())
numbers = []
for i in range(n):
    tmp = [int(c) for c in input().split()]
    numbers = quick_merge(numbers, tmp)

print(*numbers)

# 13.5 Is Valid Triangle? ----------------------------------------------------#
def is_valid_triangle(side1, side2, side3):
    if side1 + side2 > side3 and side1 + side3 > side2 and side2 + side3 > side1:
        return True
    else:
        return False


a, b, c = int(input()), int(input()), int(input())

print(is_valid_triangle(a, b, c))

# 13.5 Is a Number Prime?  ---------------------------------------------------#
def is_prime(num):
    if num == 1:
        return False

    for i in range(2, num):
        if num % i == 0:
            return False

    return True


n = int(input())

print(is_prime(n))

# 13.5 Next Prime  -----------------------------------------------------------#
def is_prime(num):
    if num == 1:
        return False
    for i in range(2, num):
        if num % i == 0:
            return False
    return True


def get_next_prime(num):
    while True:
        num += 1
        if is_prime(num):
            return num


n = int(input())

print(get_next_prime(n))

# 13.5 Good password  --------------------------------------------------------#
def is_password_good(password):
    count = 0

    if len(password) < 8:
        return False

    upper, lower, digit = False, False, False

    for char in password:
        if char.isupper():
            upper = True
        elif char.islower():
            lower = True
        elif char.isdigit():
            digit = True

    if upper and lower and digit:
        return True
    else:
        return False


txt = input()

print(is_password_good(txt))

# 13.5 Ровно в одном  --------------------------------------------------------#
def is_one_away(word1, word2):
    if len(word1) != len(word2):
        return False

    count = 0

    for i in range(len(word1)):
        if word1[i] != word2[i]:
            count += 1
        if count > 1:
            return False

    if count == 0:
        return False

    return True


txt1 = input()
txt2 = input()

print(is_one_away(txt1, txt2))

# 13.5 Палиндром  ------------------------------------------------------------#
def is_palindrome(text):
    chars = [',', '.', '!', '?', '-', ' ']

    for i in chars:
        text = text.replace(i, '')
    text = text.lower()

    if text == text[::-1]:
        return True
    else:
        return False


txt = input()

print(is_palindrome(txt))

# 13.5 BEEGEEK  --------------------------------------------------------------#
def is_prime(num):
    if num == 1:
        return False

    for i in range(2, num):
        if num % i == 0:
            return False

    return True


def is_valid_password(password):
    password = password.split(':')

    if len(password) != 3:
        return False

    if password[0] != password[0][::-1] or not is_prime(int(password[1])) or int(password[2]) % 2 != 0:
        return False

    return True


psw = input()

print(is_valid_password(psw))

# 13.5 Правильная скобочная последовательность  ------------------------------#
def is_correct_bracket(text):
    count = 0

    for i in text:
        if i == '(':
            count += 1
        elif i == ')':
            count -= 1

        if count < 0:
            return False

    if count == 0:
        return True
    else:
        return False


txt = input()

print(is_correct_bracket(txt))

# 13.5 Змеиный регистр  ------------------------------------------------------#
def convert_to_python_case(text):
    words = []

    for i in range(len(text) - 1, -1, -1):
        if text[i].isupper():
            words.append(text[i:])
            text = text[:i]

    words.reverse()
    words = '_'.join(words)

    return words.lower()


txt = input()

print(convert_to_python_case(txt))

# 13.6 Середина отрезка  -----------------------------------------------------#
def get_middle_point(x1, y1, x2, y2):
    x3 = (x1 + x2) / 2
    y3 = (y1 + y2) / 2

    return x3, y3


x_1, y_1 = int(input()), int(input())
x_2, y_2 = int(input()), int(input())

x, y = get_middle_point(x_1, y_1, x_2, y_2)
print(x, y)

# 13.6 Площадь и длина  ------------------------------------------------------#
import math

def get_circle(radius):
    c = 2 * math.pi * radius
    s = math.pi * (radius ** 2)

    return c, s


r = float(input())

length, square = get_circle(r)
print(length, square)

# 13.6 Корни уравнения  ------------------------------------------------------#
def solve(a, b, c):
    d = b ** 2 - 4 * a * c

    x1 = (-b - d ** 0.5) / (2 * a)
    x2 = (-b + d ** 0.5) / (2 * a)

    return min(x1, x2), max(x1, x2)


a, b, c = int(input()), int(input()), int(input())

x1, x2 = solve(a, b, c)
print(x1, x2)

