#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------------ Глава 9 -------------------------------------#

# 9.1 В столбик 1 ------------------------------------------------------------#
string = input()
for i in range(0, len(string), 2):
    print(string[i])

# 9.1 В столбик 2 ------------------------------------------------------------#
string = input()
for i in range(len(string) - 1, -1, -1):
    print(string[i])

# 9.1 ФИО --------------------------------------------------------------------#
name, surname, patronymic = input(), input(), input()
print(surname[0], name[0], patronymic[0], sep='')

# 9.1 Цифра 1 ----------------------------------------------------------------#
num = input()
summ = 0
for i in range(len(num)):
    summ += int(num[i])
print(summ)

# 9.1 Цифра 2 ----------------------------------------------------------------#
string = input()

digits = 0

for i in range(len(string)):
    if string[i] in '0123456789':
        digits = 1
        break

if digits == 0:
    print('Цифр нет')
else:
    print('Цифра')

# 9.1 Сколько раз? -----------------------------------------------------------#
string = input()

count_summ = 0
count_mult = 0

for i in range(len(string)):
    if string[i] == '+':
        count_summ += 1
    if string[i] == '*':
        count_mult += 1

print('Символ + встречается', count_summ, 'раз')
print('Символ * встречается', count_mult, 'раз')

# 9.1 Одинаковые соседи ------------------------------------------------------#
string = input()

count = 0

for i in range(len(string) - 1):
    if string[i] == string[i + 1]:
        count += 1

print(count)

# 9.1 Гласные и согласные ----------------------------------------------------#
string = input()

vowels = 0
consonants = 0

for i in range(len(string)):
    if string[i] in 'ауоыиэяюёеАУОЫИЭЯЮЁЕ':
        vowels += 1
    if string[i] in 'бвгджзйклмнпрстфхцчшщБВГДЖЗЙКЛМНПРСТФХЦЧШЩ':
        consonants += 1

print('Количество гласных букв равно', vowels)
print('Количество согласных букв равно', consonants)

# 9.1 Decimal to Binaryе -----------------------------------------------------#
num = int(input())

result = ''

while num > 0:
    result = str(num % 2) + result
    num //= 2
print(result)

# 9.2 Палиндром --------------------------------------------------------------#
string = input()
if string == string[::-1]:
    print('YES')
else:
    print('NO')

# 9.2 Делаем срезы 1 ---------------------------------------------------------#
string = input()

print(len(string))
print(string * 3)
print(string[0])
print(string[:3])
print(string[-3:])
print(string[::-1])
print(string[1:- 1])

# 9.2 Делаем срезы 2 ---------------------------------------------------------#
string = input()

print(string[2])
print(string[-2])
print(string[:5])
print(string[:-2])
print(string[::2])
print(string[1::2])
print(string[::-1])
print(string[::-2])

# 9.2 Две половинки ----------------------------------------------------------#
string = input()

first_half = second_half = len(string) // 2
if len(string) % 2 == 1:
    first_half += 1
    second_half += 1

print(string[second_half:], string[:first_half], sep='')

# 9.3 Заглавные буквы --------------------------------------------------------#
string = input()
if string == string.title():
    print('YES')
else:
    print('NO')

# 9.3 sWAP cASE --------------------------------------------------------------#
string = input()
print(string.swapcase())

# 9.3 Хороший оттенок --------------------------------------------------------#
string = input()
if 'хорош' in string.lower():
    print('YES')
else:
    print('NO')
    
# 9.3 Нижний регистр ---------------------------------------------------------#
string = input()
count = 0
for i in range(len(string)):
    if string[i] in 'abcdefghijklmnopqrstuvwxyz':
        count += 1
print(count)

# 9.4 Количество слов --------------------------------------------------------#
string = input()
print(string.count(' ') + 1)

# 9.4 Минутка генетики -------------------------------------------------------#
string = input()

string = string.lower()
print('Аденин:', string.count('а'))
print('Гуанин:', string.count('г'))
print('Цитозин:', string.count('ц'))
print('Тимин:', string.count('т'))

# 9.4 Очень странные дела ----------------------------------------------------#
num = int(input())

count = 0

for i in range(num):
    string = input()
    tmp = string.count('11')
    if tmp > 2:
        count += 1

print(count)

# 9.4 Количество цифр --------------------------------------------------------#
string = input()

count = 0

for i in range(10):
    count += string.count(str(i))
print(count)

# 9.4 .com or .ru ------------------------------------------------------------#
domain = input()

if domain.endswith('.com') or domain.endswith('.ru'):
    print('YES')
else:
    print('NO')

# 9.4 Самый частотный символ -------------------------------------------------#
string = input()

max_count = 0
char =''

for i in string:
    count = string.count(i)
    if count >= max_count:
        max_count = count
        char = i
print(char)

# 9.4 Первое и последнее вхождение -------------------------------------------#
string = input()

first = string.find('f')
last = string.rfind('f')

if first < 0:
    print('NO')
else:
    print(first, end=' ')
    if first < last:
        print(last)

# 9.4 Удаление фрагмента -----------------------------------------------------#
string = input()

first = string.find('h')
last = string.rfind('h')

print(string[:first] + string[last+1:])

# 9.6 Символы в диапазоне ----------------------------------------------------#
a, b = int(input()), int(input())

for i in range(a, b + 1):
    print(chr(i), end=' ')

# 9.6 Простой шифр -----------------------------------------------------------#
string = input()
for i in string:
    print(ord(i), end=' ')
    
# 9.6 Шифр Цезаря ------------------------------------------------------------#
n, string = int(input()), input()

for i in string:
    num = ord(i) - n
    if num < 97:
        num += 26

    print(chr(num), end='')
    
