#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------------ Глава 4 -------------------------------------#

# 4.1 Пароль -----------------------------------------------------------------#
passwd, confirm_passwd = input(), input()

if passwd == confirm_passwd:
    print('Пароль принят')
else:
    print('Пароль не принят')
    
# 4.1 Четное или нечетное? ---------------------------------------------------#
num = int(input())

if num % 2 == 0:
    print('Четное')
else:
    print('Нечетное')

# 4.1 Четное или нечетное? ---------------------------------------------------#
num    = int(input())

first  =  num // 1000
second = (num // 100) % 10
third  = (num // 10) % 10
fourth =  num % 10

if first + fourth == second - third:
    print('ДА')
else:
    print('НЕТ')

# 4.1 Роскомнадзор -----------------------------------------------------------#
age = int(input())

if age >= 18:
    print('Доступ разрешен')
else:
    print('Доступ запрещен')

# 4.1 Арифметическая прогрессия ----------------------------------------------#
first, second, third = int(input()), int(input()), int(input())

if second == (first + third) / 2:
    print('YES')
else:
    print('NO')

# 4.1 Наименьшее из двух чисел -----------------------------------------------#
first, second = int(input()), int(input())

if first < second: 
    print(first)
else:
    print(second)

# 4.1 Наименьшее из двух чисел -----------------------------------------------#
first, second, third, fourth = int(input()), int(input()), int(input()), int(input())

if first > second: 
    first = second
if first > third: 
    first = third
if first > fourth: 
    first = fourth

print(first)

# 4.1 Возрастная группа ------------------------------------------------------#
age = int(input())

if age <= 13:
    print('детство')
if 14 <= age <= 24:
    print('молодость')
if 25 <= age <= 59:
    print('зрелость')
if age > 59:
    print('старость')

# 4.1 Только +  --------------------------------------------------------------#
first, second, c = int(input()), int(input()), int(input())

if first < 0:
    first = 0
if second < 0:
    second = 0    
if third < 0:
    third = 0
print(first + second + third)

# 4.2 Принадлежность 1  ------------------------------------------------------#
num = int(input())

if -1 < num < 17:
    print('Принадлежит')
else:
    print('Не принадлежит')

# 4.2 Принадлежность 2  ------------------------------------------------------#
num = int(input())

if -3 >= num or num >= 7:
    print('Принадлежит')
else:
    print('Не принадлежит')

# 4.2 Принадлежность 3  ------------------------------------------------------#
num = int(input())
if -30 < num <= -2 or 7 < num <= 25:
    print('Принадлежит')
else:
    print('Не принадлежит')

# 4.2 Красивое число  --------------------------------------------------------#
num = int(input())

if 1000 < num < 9999:
    if num % 7 == 0 or num % 17 == 0:
        print('YES')
    else:
        print('NO')
else:
    print('NO')

# 4.2 Неравенство треугольника  ----------------------------------------------#
a, b, c = int(input()), int(input()), int(input())

if a + b > c and a + c > b and b + c > a:
    print('YES')
else:
    print('NO')

# 4.2 Високосный год  --------------------------------------------------------#
year = int(input())

if year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
    print('YES')
else:
    print('NO')

# 4.2 Ход ладьи  -------------------------------------------------------------#
x1, y1, x2, y2 = int(input()), int(input()), int(input()), int(input())

if x1 == x2 or y1 == y2:
    print('YES')
else:
    print('NO')

# 4.2 Ход короля  ------------------------------------------------------------#
x1, y1, x2, y2 = int(input()), int(input()), int(input()), int(input())

if -1 <= x2 - x1 <= 1 and -1 <= y2 - y1 <= 1:
    print('YES')
else:
    print('NO')
    
# 4.3 Гонка спидстеров  ------------------------------------------------------#
n, k = int(input()), int(input())

if n > k:
    print('NO')
elif n == k:
    print('Don\'t know')
else:
    print('YES') 

# 4.3 Вид треугольника  ------------------------------------------------------#
a, b, c = int(input()), int(input()), int(input())

if a == b == c:
    print('Равносторонний')
elif a != b and a != c and b != c:
    print('Разносторонний')
else:
    print('Равнобедренный')

# 4.3 Среднее число  ---------------------------------------------------------#
num0, num1, num2 = int(input()), int(input()), int(input())

if num0 < num1 < num2 or num2 < num1 < num0:
    print(num1)
elif num1 < num0 < num2 or num2 < num0 < num1:
    print(num0)
else:
    print(num2)

# 4.3 Количество дней  -------------------------------------------------------#
month = int(input())

if month == 2:
    print(28)
elif month == 4 or month == 6 or month == 9 or month == 11:
    print(30)
else:
    print(31)

# 4.3 Церемония взвешивания  -------------------------------------------------#
weight  = int(input())

if weight  < 60:
    print('Легкий вес')
elif 60 <= weight  < 64:
    print('Первый полусредний вес')
else:
    print('Полусредний вес')
    
# 4.3 Самописный калькулятор  ------------------------------------------------#
num0, num1, sign = int(input()), int(input()), input()

if sign == '+':
    print(num0 + num1)
elif sign == '-':
    print(num0 - num1)
elif sign == '*':
    print(num0 * num1)
elif sign == '/':
    if num1 == 0:
        print('На ноль делить нельзя!')
    else:
        print(num0 / num1)
else:
    print('Неверная операция')

# 4.3 Цветовой микшер  -------------------------------------------------------#
first, second = input(), input()

if first == 'красный':
    if second == 'синий':
        print('фиолетовый')    
    elif second == 'желтый':
        print('оранжевый')      
    elif second == 'красный':
        print('красный')        
    else:
        print('ошибка цвета')    
elif first == 'синий':
    if second == 'красный':
        print('фиолетовый')    
    elif second == 'желтый':
        print('зеленый')      
    elif second == 'синий':
        print('синий')  
    else:
        print('ошибка цвета')
elif first == 'желтый':
    if second == 'синий':
        print('зеленый')    
    elif second == 'красный':
        print(' оранжевый')      
    elif second == 'желтый':
        print('желтый') 
    else:
        print('ошибка цвета')
else:
    print('ошибка цвета')

# 4.3 Цвета колеса рулетки  --------------------------------------------------#
num = int(input())

if num == 0:
    print('зеленый')
elif  (num < 0) or (num  > 36):
    print('ошибка ввода')
elif (num % 2) == 0:
    if (1 <= num <= 10) or (19 <= num <= 28):
        print('черный')
    else:
        print('красный')
else:
    if (1 <= num <= 10) or (19 <= num <= 28):
        print('красный')
    else:
        print('черный')

# 4.3 Пересечение отрезков  --------------------------------------------------#
a1, b1, a2, b2 = int(input()), int(input()), int(input()), int(input())

if a2 < a1:
    if b2 < a1:
        print('пустое множество')
    elif b2 == a1:
        print(b2)
    elif a1 < b2 <= b1:
        print(a1, b2)
    elif b2 > b1:
        print(a1, b1)
elif a2 == a1:
    if b2 <= b1:
        print(a2, b2)
    else:
        print(a2, b1)
elif a2 < b1:
    if b2 <= b1:
        print(a2, b2)
    else:
        print(a2, b1)
elif a2 == b1:
    print(a2)
else:
    print('пустое множество')

