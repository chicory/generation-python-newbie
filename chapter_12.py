#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------  Раздел 12 - Экзамен ------------------------------#

# 12.2 Список четных ---------------------------------------------------------#
numbers = []
for i in range(2, int(input()) + 1, 2):
    numbers.append(i)
print(numbers)

# 12.2 Сумма двух списков ----------------------------------------------------#
l = input().split()
m = input().split()

result = []

for i in range(len(l)):
    result.append(int(l[i]) + int(m[i]))

print(*result)

# 12.2 Сумма чисел -----------------------------------------------------------#
numbers = input().split()
numbers = [int(i) for i in numbers]
print(*numbers, sep='+', end='')
print(f'={sum(numbers)}')

# 12.2 Валидный номер --------------------------------------------------------#
number = [i for i in input().split('-') if i.isdigit()]

is_valid = 'YES'

if len(number) < 3 or len(number) > 4:
    is_valid = 'NO'
else:
    if len(number) == 4 and number[0] != '7':
        is_valid = 'NO'

    if len(number[-1]) != 4 or len(number[-2]) != 3 or len(number[-3]) != 3:
        is_valid = 'NO'

print(is_valid)

# 12.2 Самый длинный ---------------------------------------------------------#
str_len = [len(i) for i in input().split()]
print(max(str_len))

# 12.2 Молодежный жаргон -----------------------------------------------------#
print(*[i[1:] + i[:1] + 'ки' for i in input().split()])

