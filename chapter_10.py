#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------  Раздел 10 - Экзамен ------------------------------#

# 10.2 Каждый третий ---------------------------------------------------------#
string = input()

for i in range(len(string)):
    if i % 3 != 0:
        print(string[i], end='')

# 10.2 Замени меня полностью -------------------------------------------------#
string = input()
print(string.replace('1', 'one'))

# 10.2 Удали меня полностью --------------------------------------------------#
string = input()
print(string.replace('@', ''))

# 10.2 Второе вхождение ------------------------------------------------------#
string = input()

count = string.count('f')

if count == 0:
    print(-2)
elif count == 1:
    print(-1)
else:
    string = string.replace('f', 'a', 1)
    print(string.find('f'))

# 10.2 Переворот -------------------------------------------------------------#
string = input()

first = string.find('h')
last = string.rfind('h')

print(string[:first] + string[last:first:-1] + string[last:])

