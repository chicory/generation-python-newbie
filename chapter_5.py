#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------  Раздел 5 - Экзамен -------------------------------#

# 5.1 Начало столетия --------------------------------------------------------#
num = int(input())

if num % 100 == 0:
    print('YES')
else:
    print('NO')

# 5.1 Шахматная доска --------------------------------------------------------#
a1, b1, a2, b2 = int(input()), int(input()), int(input()), int(input())

color1 = (a1 + b1) % 2
color2 = (a2 + b2) % 2
if color1 == color2:
    print('YES')
else:
    print('NO')

# 5.1 Girls only -------------------------------------------------------------#
age, sex = int(input()), input()

if sex == 'f' and 10 <= age <= 15:
    print('YES')
else:
    print('NO')

# 5.1 Римские цифры ----------------------------------------------------------#
num = int(input())

if num == 1:
    print('I')
elif num == 2:
    print('II')
elif num == 3:
    print('III')
elif num == 4:
    print('IV')
elif num == 5:
    print('V')
elif num == 6:
    print('VI')
elif num == 7:
    print('VII')
elif num == 8:
    print('VIII')
elif num == 9:
    print('IX')
elif num == 10:
    print('X')
else:
    print('ошибка')

# 5.1 YES or NO вот в чем вопрос ---------------------------------------------#
n = int(input())

if num % 2 == 0:
    if 2 <= num <= 5:
        print('NO')
    elif 6 <= num <= 20:
        print('YES')
    else:
        print('NO')
else:
    print('YES')

# 5.1 Ход слона --------------------------------------------------------------#
a1, b1, a2, b2 = int(input()), int(input()), int(input()), int(input())

if (a1 - b1 == a2 - b2) or (a1 + b1 == a2 + b2):
    print('YES')
else:
    print('NO')

# 5.1 Ход коня ---------------------------------------------------------------#
x1, y1, x2, y2 = int(input()), int(input()), int(input()), int(input())

if (x1 - x2) ** 2 + (y1 - y2) ** 2 == 5:
    print("YES")
else:
    print("NO")

# 5.1 Ход ферзя --------------------------------------------------------------#
x1, y1, x2, y2 = int(input()), int(input()), int(input()), int(input())
 
if abs(x1 - x2) == abs(y1 - y2) or x1 == x2 or y1 == y2:
    print('YES')
else:
    print('NO')

