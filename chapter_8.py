#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------  Раздел 8 - Экзамен -------------------------------#

# 8.2 Ревью кода - 7 ---------------------------------------------------------#
n = int(input())
s = 0
while n > 0:
    if n % 2 == 0:
        s += n % 10
    n //= 10
print(s)

# 8.2 Ревью кода - 8 ---------------------------------------------------------#
n = 8
count = 0
maximum = -10**6 - 1
for i in range(1, n + 1):
    x = int(input())
    if x % 4 == 0:
        count += 1
        if x > maximum:
            maximum = x
if count > 0:
    print(count)
    print(maximum)
else:
    print('NO')
    
# 8.2 Ревью кода - 9 ---------------------------------------------------------#
n = 4
count = 0
maximum = -10 ** 6 -1
for i in range(1, n + 1):
    x = int(input())
    if x % 2 != 0:
        count += 1
        if x > maximum:
            maximum = x
if count > 0:
    print(count)
    print(maximum)
else:
    print('NO')

# 8.2 Звездная рамка ---------------------------------------------------------#
rows = int(input())
for i in range(1, rows + 1):
    if i == 1 or i == rows:
        print('*******************')
    else:
        print('*                 *')

# 8.2 Третья цифра -----------------------------------------------------------#
num = int(input())

while num > 999:
    num //= 10

print(num % 10)

# 8.2 Все вместе 2 -----------------------------------------------------------#
num = int(input())

three_count = 0
last_digit = num % 10
last_digit_count = 0
even_count = 0
greater_fife_summ = 0
greater_seven_mult = 1
zero_five_count = 0

while num > 0:
    digit = num % 10
    if digit == 3:
        three_count += 1
    if digit == last_digit:
        last_digit_count += 1
    if digit % 2 == 0:
        even_count += 1
    if digit > 5:
        greater_fife_summ += digit
    if digit > 7:
        greater_seven_mult *= digit
    if digit == 0 or digit == 5:
        zero_five_count += 1
    num //= 10

print(three_count, last_digit_count, even_count, greater_fife_summ, greater_seven_mult, zero_five_count, sep='\n')


