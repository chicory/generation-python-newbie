#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------------ Глава 6 -------------------------------------#

# 6.1 Площадь треугольника ---------------------------------------------------#
a, b = float(input()), float(input())
print(0.5 * a * b)

# 6.1 Две старушки -----------------------------------------------------------#
s, v1, v2 = float(input()), float(input()), float(input())
print(s / (v1 + v2))

# 6.1 Обратное число ---------------------------------------------------------#
num = float(input())

if num == 0:
    print('Обратного числа не существует')    
else:
    print(num ** -1)

# 6.1 451 градус по Фаренгейту  ----------------------------------------------#
f = float(input())
print(5 / 9 * (f - 32))

# 6.1 Dog age  ---------------------------------------------------------------#
age = float(input())
if age <= 2:
    print(age * 10.5)
else:
    print((age - 2) * 4 + 21)   

# 6.1 Первая цифра после точки  ----------------------------------------------#
num = float(input())
print(int(num * 10 % 10 ))

# 6.1 Дробная часть  ---------------------------------------------------------#
num = float(input())
print(num % 1 )

# 6.1 Наибольшее и наименьшее  -----------------------------------------------#
num0, num1, num2, num3, num4 = int(input()), int(input()), int(input()), int(input()), int(input())
print('Наименьшее число =', min(num0, num1, num2, num3, num4))
print('Наибольшее число =', max(num0, num1, num2, num3, num4))

# 6.1 Сортировка трёх  -------------------------------------------------------#
num0, num1, num2 = int(input()), int(input()), int(input())
out3 = min(num0, num1, num2)
out1 = max(num0, num1, num2)
out2 = (num0 + num1 + num2) - (out1 + out3)
print(out1, out2, out3, sep='\n')

# 6.1 Интересное число  ------------------------------------------------------#
num = int(input())

num1 = num // 100 
num2 = (num % 100) // 10
num3 = num % 10

n_min = min(num1, num2, num3)
n_max = max(num1, num2, num3)

n_mid = (num1 + num2 + num3) - (n_max + n_min)

if (n_max - n_min) == n_mid:
    print('Число интересное')
else:
    print('Число неинтересное')

# 6.1 Абсолютная сумма  ------------------------------------------------------#
a1, a2 , a3, a4, a5 = float(input()), float(input()), float(input()), float(input()), float(input())
print(abs(a1) + abs(a2) + abs(a3) + abs(a4) + abs(a5))

# 6.1 Манхэттенское расстояние  ----------------------------------------------#
p1, p2, q1, q2 = int(input()), int(input()), int(input()),int(input())
print(abs((p1 - q1)) + abs((p2 - q2)))

# 6.1 Евклидово расстояние  --------------------------------------------------#
from math import sqrt

x1, y1, x2, y2 = float(input()), float(input()), float(input()), float(input())
p = sqrt(((x1 - x2) ** 2) + ((y1 - y2) ** 2))
print(p)

# 6.1 Площадь и длина  -------------------------------------------------------#
import math

r = float(input())
print(math.pi * (r ** 2), math.pi * 2 * r, sep='\n')

# 6.1 Средние значения  ------------------------------------------------------#
from math import sqrt

a, b = float(input()), float(input())
print((a + b) / 2)
print(sqrt(a * b))
print((2 * a * b) / (a + b))
print(sqrt((a ** 2 + b ** 2) / 2))

# 6.1 Тригонометрическое выражение  ------------------------------------------#
import math

x = float(input())
x = math.radians(x)
print(math.sin(x) + math.cos(x) + (math.tan(x) * math.tan(x)))

# 6.1 Пол и потолок  ---------------------------------------------------------#
from math import ceil, floor

x = float(input())
print(floor(x) + ceil(x))

# 6.1 Квадратное уравнение  --------------------------------------------------#
import math
a, b, c = float(input()), float(input()), float(input())
d = b ** 2 - 4 * a * c

if d < 0:
    print('Нет корней')
elif d == 0:
    print(-b / (2 * a))
elif d > 0:
    x1 = (-b - d ** 0.5) / (2 * a)
    x2 = (-b + d ** 0.5) / (2 * a)
    print(min(x1, x2))
    print(max(x1, x2))

# 6.1 Квадратное уравнение  --------------------------------------------------#
from math import *

n, a = float(input()), float(input())
out = (n * pow(a, 2)) / (4 * tan(pi / n))
print(out)

# 6.1 Напишите программу, которая выводит текст  -----------------------------#
print('"Python is a great language!", said Fred. ' + '"I don\'t ever remember having this much fun before."')

# 6.1 What's Your Name?  -----------------------------------------------------#
name, surname = input(), input()
print('Hello ' + name + ' ' + surname + '! You just delved into Python')

# 6.1 Футбольная команда  ----------------------------------------------------#
name = input()
print('Футбольная команда', name, 'имеет длину', len(name), 'символов')

# 6.1 Три города  ------------------------------------------------------------#
c1, c2, c3 = input(), input(), input()

min_len = min(len(c1), len(c2), len(c3))
max_len = max(len(c1), len(c2), len(c3))

if min_len == len(c1):
    print(c1)
elif min_len == len(c2):
    print(c2)
else:
    print(c3)

if max_len == len(c1):
    print(c1)
elif max_len == len(c2):
    print(c2)
else:
    print(c3)
    
# 6.1 Арифметические строки  -------------------------------------------------#
str0, str1, str2 = len(input()), len(input()), len(input())

if ((2 * str1 - str2 - str0) * (2 * str2 - str1 - str0) * (2 * str0 - str1 - str2)) == 0:
    print('YES')
else:
    print('NO')

# 6.1 Цвет настроения синий  -------------------------------------------------#
string = input()

if 'синий' in string:
    print('YES')
else:
    print('NO')

# 6.1 Отдыхаем ли?  ----------------------------------------------------------#
string = input()

if 'суббота' in string or 'воскресенье' in string:
    print('YES')
else:
    print('NO')

# 6.1 Корректный email  ------------------------------------------------------#
email = input()

if '@' in email and '.' in email:
    print('YES')
else:
    print('NO')

