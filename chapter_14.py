#-----------------------------------------------------------------------------#
#                                                                             #
#   Решения для практических задач "Поколение Python": курс для начинающих.   #
#   Git:       https://codeberg.org/chicory/generation-python-newbie          #
#   Copyright: chicory@disroot.org 2021                                       #  
#   License:   MIT https://opensource.org/licenses/MIT                        # 
#                                                                             # 
#-----------------------------------------------------------------------------#

# ------------------------  Раздел 14 - Экзамен ------------------------------#

# 14.1 Звездный треугольник --------------------------------------------------#
def draw_triangle():
    for i in range(8):
        print(' ' * (8 - 1 - i) + '*' * (1 + i * 2))


draw_triangle()

# 14.1 Калькулятор доставки --------------------------------------------------#
def get_shipping_cost(quantity):
    return 1000 + (quantity - 1) * 120


n = int(input())

print(get_shipping_cost(n))

# 14.1 Биномиальный коэффициент ----------------------------------------------#
from math import factorial

def compute_binom(n, k):
    if k <= n:
        return int(factorial(n) / (factorial(k) * factorial(n - k)))
    else:
        return 0


n = int(input())
k = int(input())

print(compute_binom(n, k))

# 14.1 Число словами ---------------------------------------------------------#
def number_to_words(num):
    units = ['', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять', 'десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать']
    tens = ['', 'десять', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто']

    if num < 20:
        return units[num]
    elif num % 10 == 0:
        return tens[num // 10]
    else:
        return tens[num // 10] + ' ' + units[num % 10]


n = int(input())

print(number_to_words(n))

# 14.1 Искомый месяц ---------------------------------------------------------#
def get_month(language, number):
    en = ['', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
    ru = ['', 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь']

    if language == 'ru':
        return ru[number]
    else:
        return en[number]


lan = input()
num = int(input())

print(get_month(lan, num))

# 14.1 Магические даты -------------------------------------------------------#
def is_magic(date):
    date = date.split('.')

    return int(date[0]) * int(date[1]) == int(date[2]) % 100


date = input()

print(is_magic(date))

# 14.1 Панграммы -------------------------------------------------------------#
def is_pangram(text):
    alphabet = [chr(char) for char in range(97, 123)]
    for char in alphabet:
        if char not in text.lower():
            return False
    return True

text = input()

print(is_pangram(text))

